# Blob-Contract

Smart Contract of the Blob ERC20 token on the Ubiq Blockchain.  
First attempt to create an ERC20 token on a blockchain, I did it mainly as a test, in order to understand how it works, and... it works ^^

Name : Blob  
Symbol : BLOB  
Total supply : 65536  
Decimal : 8  

Blob token address : https://ubiqscan.io/address/0xbbcd60184a3a3ea7c3efef5f7fafca7342310298

Also deployed on the Ethereum testnet Sepolia, here, https://sepolia.etherscan.io/token/0xb69d23af8c86ad6c82c9e3ba30b4966959b92f77	